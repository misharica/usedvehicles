from django.apps import AppConfig


class SearchCarConfig(AppConfig):
    name = 'search_car'
