from django.shortcuts import render, redirect, reverse
from django.views import View
from search_car.forms import QuickSearchForm, DetailedSearchForm
from announcement_car.models import CarAnnouncement
from image_upload.models import CarImageUploadModel
from contact_info.models import AnnouncementContactInfo


# Create your views here.
class QuickSearch(View):
    def get(self, request):
        quick_search_form = QuickSearchForm()
        context = {'quick_search_form': quick_search_form}
        return render(request, 'search_car/quick_search.html', context);

    def post(self, request):
        quick_search_form = QuickSearchForm(request.POST);
        if quick_search_form.is_valid():
            q = CarAnnouncement.objects.filter(
                    car__brand = quick_search_form.cleaned_data['brand'])
            if quick_search_form.cleaned_data['model']:
                print('model')
                q = q.filter(car__model = quick_search_form.cleaned_data['model'])
            if quick_search_form.cleaned_data['body_type']:
                print('body_type')
                q = q.filter(car__body_type = quick_search_form.cleaned_data['body_type'])
            if quick_search_form.cleaned_data['price_from']:
                print('price_from')
                q = q.filter(price__gte = quick_search_form.cleaned_data['price_from'])
            if quick_search_form.cleaned_data['price_to']:
                print('price_to')
                q = q.filter(price__lte = quick_search_form.cleaned_data['price_to'])
            if quick_search_form.cleaned_data['first_reg_from']:
                print('first_reg_from')
                q = q.filter(car__first_registration__gte = quick_search_form.cleaned_data['first_reg_from'])
            if quick_search_form.cleaned_data['first_reg_to']:
                print('first_reg_to')
                q = q.filter(car__first_registration__lte = quick_search_form.cleaned_data['first_reg_to'])
            if quick_search_form.cleaned_data['fuel_type']:
                print('fuel_type')
                q = q.filter(car__fuel_type = quick_search_form.cleaned_data['fuel_type'])
            if quick_search_form.cleaned_data['region']:
                print('region')
                q = q.filter(contact_info__region = quick_search_form.cleaned_data['region'])

            images = []
            for record in q:
                images.append(CarImageUploadModel.objects.get(announcement=record, ordinal=0))

            context = {'results': q,
                        'images': images}

        # return redirect(reverse('home_page:home'))
        return render(request, 'search_car/search_results.html', context);

class DetailedSearch(View):
    def get(self, request):
        detailed_search_form = DetailedSearchForm()
        context = {'detailed_search_form': detailed_search_form}
        return render(request, 'search_car/detailed_search.html', context);

    def post(self, request):
        return render(request, 'search_car/detailed_search.html', None);

def latest_records(request):
    # TODO: when and if our db grows use some filter instead of all() !!!
    # TODO: check if images[0] exists
    records = CarAnnouncement.objects.filter(is_active=True).order_by('date_updated').reverse()[0:12]
    images = []
    for record in records:
        images.append(CarImageUploadModel.objects.get(announcement=record, ordinal=0))
    context = {'records': records,
                'images': images}
    return render(request, 'search_car/latest_records.html', context)
