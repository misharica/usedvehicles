import datetime
from django import forms
from car.models import Brand, Model, BodyType, FuelType, Transmission, Drive, \
                        EmissionClass, Color, AirCondition, CruiseControl, Airbag
from contact_info.models import Region

class QuickSearchForm(forms.Form):
    """ Form for quick car search """
    brand = forms.ModelChoiceField(empty_label = 'Brand...',
            widget=forms.Select, queryset=Brand.objects.all())
    model = forms.ModelChoiceField(required=False, empty_label = 'Model...',
            widget=forms.Select, queryset=Model.objects.all())
    body_type = forms.ModelChoiceField(required=False, empty_label = 'Body type...',
            widget=forms.Select, queryset=BodyType.objects.all())
    price_from = forms.IntegerField(required=False, min_value=0)
    price_to = forms.IntegerField(required=False, min_value=0)
    first_reg_from = forms.IntegerField(required=False,
            min_value=1900, max_value=datetime.datetime.now().year)
    first_reg_to = forms.IntegerField(required=False,
            min_value=1900, max_value=datetime.datetime.now().year)
    km_from = forms.IntegerField(required=False, min_value=0)
    km_to = forms.IntegerField(required=False, min_value=0)
    fuel_type = forms.ModelChoiceField(required=False, empty_label = 'Fuel type...',
            widget=forms.Select, queryset=FuelType.objects.all())
    region = forms.ModelChoiceField(required=False, empty_label = 'Region...',
            widget=forms.Select, queryset=Region.objects.all())

    def __init__(self, *args, **kwargs):
        super(QuickSearchForm, self).__init__(*args, **kwargs)
        fields_list = {'brand': 'Brand',
                        'model': 'Model',
                        'body_type': 'Body type',
                        'price_from': 'Price from',
                        'price_to': 'Price to',
                        'first_reg_from': 'First registration from',
                        'first_reg_to': 'First registration to',
                        'km_from': 'Kilometerage from',
                        'km_to': 'Kilometerage to',
                        'fuel_type': 'FuelType',
                        'region': 'Region'}

        self.fields['model'].widget.attrs['disabled'] = "disabled"

        for key in fields_list:
            self.fields[key].widget.attrs.update({'class': 'form-control'})
            if self.fields[key].widget.input_type == 'number':
                self.fields[key].widget.attrs.update({'placeholder': fields_list[key]})

class DetailedSearchForm(forms.Form):
        """ Form for detailed car search """
        # quick search
        brand = forms.ModelChoiceField(empty_label = 'Brand...',
                widget=forms.Select, queryset=Brand.objects.all())
        model = forms.ModelChoiceField(required=False, empty_label = 'Model...',
                widget=forms.Select, queryset=Model.objects.all())
        body_type = forms.ModelChoiceField(required=False, empty_label = 'Body type...',
                widget=forms.Select, queryset=BodyType.objects.all())
        price_from = forms.IntegerField(required=False, min_value=0)
        price_to = forms.IntegerField(required=False, min_value=0)
        first_reg_from = forms.IntegerField(required=False,
                min_value=1900, max_value=datetime.datetime.now().year)
        first_reg_to = forms.IntegerField(required=False,
                min_value=1900, max_value=datetime.datetime.now().year)
        km_from = forms.IntegerField(required=False, min_value=0)
        km_to = forms.IntegerField(required=False, min_value=0)
        fuel_type = forms.ModelChoiceField(required=False, empty_label = 'Fuel type...',
                widget=forms.Select, queryset=FuelType.objects.all())
        region = forms.ModelChoiceField(required=False, empty_label = 'Region...',
                widget=forms.Select, queryset=Region.objects.all())

        # detailed search
        ccm_from = forms.IntegerField(required=False, min_value=0)
        ccm_to = forms.IntegerField(required=False, min_value=0)
        power_from = forms.IntegerField(required=False, min_value=0)
        power_to = forms.IntegerField(required=False, min_value=0)
        transmission = forms.ModelChoiceField(required=False, empty_label = 'Transmission...',
                widget=forms.Select, queryset=Transmission.objects.all())
        drive = forms.ModelChoiceField(required=False, empty_label = 'Drive...',
                widget=forms.Select, queryset=Drive.objects.all())
        emission_class = forms.ModelChoiceField(required=False, empty_label = 'Emission class...',
                widget=forms.Select, queryset=EmissionClass.objects.all())
        door_num = forms.IntegerField(required=False, min_value=0)
        seats_number = forms.IntegerField(required=False, min_value=0)
        color = forms.ModelChoiceField(required=False, empty_label = 'Color...',
                widget=forms.Select, queryset=Color.objects.all())
        metalic_color = forms.BooleanField(required=False)
        ac = forms.ModelChoiceField(required=False, empty_label = 'AirCondition...',
                widget=forms.Select, queryset=AirCondition.objects.all())
        cruise_ctrl = forms.ModelChoiceField(required=False, empty_label = 'Cruse control...',
                widget=forms.Select, queryset=CruiseControl.objects.all())
        airbags = forms.ModelChoiceField(required=False, empty_label = 'Airbags...',
                widget=forms.Select, queryset=Airbag.objects.all())

        def __init__(self, *args, **kwargs):
            super(DetailedSearchForm, self).__init__(*args, **kwargs)
            fields_list = {'brand': 'Brand',
                            'model': 'Model',
                            'body_type': 'Body type',
                            'price_from': 'Price from',
                            'price_to': 'Price to',
                            'first_reg_from': 'First registration from',
                            'first_reg_to': 'First registration to',
                            'km_from': 'Kilometerage from',
                            'km_to': 'Kilometerage to',
                            'fuel_type': 'FuelType',
                            'region': 'Region',
                            'ccm_from': 'Cubic capacity from',
                            'ccm_to': 'Cubic capacity to',
                            'power_from': 'Power from',
                            'power_to': 'Power to',
                            'transmission': 'Transmission',
                            'drive': 'Drive',
                            'emission_class': 'Emission class',
                            'door_num': 'Door number',
                            'seats_number': 'Seats number',
                            'color': 'Color',
                            'ac': 'AirCondition',
                            'cruise_ctrl': 'Cruise control',
                            'airbags': 'Airbags'}

            self.fields['model'].widget.attrs['disabled'] = "disabled"
            self.fields['metalic_color'].label = 'Metalic'

            for key in fields_list:
                self.fields[key].widget.attrs.update({'class': 'form-control'})
                if self.fields[key].widget.input_type == 'number':
                    self.fields[key].widget.attrs.update({'placeholder': fields_list[key]})
