from django import template
register = template.Library()

@register.filter
def get_image_url(images, i):
    return images[i].image.url
