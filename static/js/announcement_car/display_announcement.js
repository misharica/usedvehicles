function scrollThumnails() {
  var activeThumbnail = $(".img-thumbnail-active");
  var all_thumbnails_cont = $(".all-images-container");
  var all_thumbnails = $(".all-images-container .all-images");
  var oldX = all_thumbnails.css("transform").match(/matrix(?:(3d)\(-{0,1}\d+\.?\d*(?:, -{0,1}\d+\.?\d*)*(?:, (-{0,1}\d+\.?\d*))(?:, (-{0,1}\d+\.?\d*))(?:, (-{0,1}\d+\.?\d*)), -{0,1}\d+\.?\d*\)|\(-{0,1}\d+\.?\d*(?:, -{0,1}\d+\.?\d*)*(?:, (-{0,1}\d+\.?\d*))(?:, (-{0,1}\d+\.?\d*))\))/)[5];
  var relativePos = activeThumbnail.position().left + all_thumbnails.position().left - all_thumbnails_cont.position().left;
  var x = oldX;

  if(relativePos + activeThumbnail.width() > all_thumbnails_cont.width()) {
    var coefficient = $(".all-images-container .all-images").width() / activeThumbnail.width() - 1.05;
    var tmp = -1 * ($(".img-thumbnail-container").last().index() - coefficient) * activeThumbnail.width();
    x = oldX - activeThumbnail.width();
    x = x < tmp ? (tmp) : x; // don't translate too much left
  }
  else if(relativePos < 50) {
    x = parseInt(oldX) + activeThumbnail.width();
    x = x > 0 ? 0 : x; // don't translate too much right
  }
  all_thumbnails.css({"transform": "translate3d(" + x + "px, 0px, 0px)"});
  // all_thumbnails.css("-webkit-transition-duration", ".4s");
}

function handleKeyPresses(e) {
  switch(e.which) {
    case 37: // left
      // e.preventDefault(); // prevent the default action (scroll / move caret)
      activeDiv = $(".img-thumbnail-active");
      if(activeDiv[0].previousElementSibling) {
        activeDiv[0].classList.remove("img-thumbnail-active");
        activeDiv[0].previousElementSibling.classList.add("img-thumbnail-active");
      }
      break;
    case 39: // right
      // e.preventDefault(); // prevent the default action (scroll / move caret)
      activeDiv = $(".img-thumbnail-active");
      if(activeDiv[0].nextElementSibling) {
        activeDiv[0].classList.remove("img-thumbnail-active");
        activeDiv[0].nextElementSibling.classList.add("img-thumbnail-active");
      }
      break;
    default: return; // exit this handler for other keys
  }

  // set active image
  activeImageUrl = $(".img-thumbnail-active .img-thumbnail").attr("src");
  $(".main-image .img-fluid").attr("src", activeImageUrl);

  // scroll thumbnail images
  scrollThumnails();
}

function handleImgThumbnailClicks() {
  $(".img-thumbnail-active").removeClass("img-thumbnail-active");
  $(this).addClass("img-thumbnail-active");
  activeImageUrl = $(".img-thumbnail-active .img-thumbnail").attr("src");
  $(".main-image .img-fluid").attr("src", activeImageUrl);
  scrollThumnails();
}

$(document).ready(function(){
  // set initial states
  $(".img-thumbnail-container").first().addClass("img-thumbnail-active");
  $(".img-thumbnail-container").click(handleImgThumbnailClicks);
  $(document).keydown(handleKeyPresses);
});
