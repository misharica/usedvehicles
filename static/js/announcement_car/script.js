function testF() {
  if($(this).val() == 0) {
      opt = document.createElement('option')
      opt.text = 'Choose...'
      opt.value = 0
      $("#id_model").html(opt)
      $("#id_model").prop('disabled', true);
      return;
  }

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      $("#id_model").html(this.responseText)
    }
  };
  xhttp.open("GET", "model_dropdown_list_options?brand_id=" + $(this).val(), true);
  xhttp.send();
  $("#id_model").prop('disabled', false);
}

$(document).ready(function(){
  /* kW <=> HP transformations */
  rad = document.getElementsByName("inlineRadioOptions");
  for (var i = 0; i < rad.length; i++) {
    rad[i].addEventListener('change', function() {
      $("#powerUnits").text(this.value)
    });
  }

  /* dynamically load models */
  $("#id_brand").change(testF);


});
