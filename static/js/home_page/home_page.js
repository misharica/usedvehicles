function loadModels() {
  if($(this).val() == 0) {
      opt = document.createElement('option')
      opt.text = 'Choose...'
      opt.value = 0
      $("#id_model").html(opt)
      $("#id_model").prop('disabled', true);
      return;
  }

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      $("#id_model").html(this.responseText)
    }
  };
  xhttp.open("GET", "model_dropdown_list_options?brand_id=" + $(this).val(), true);
  xhttp.send();
  $("#id_model").prop('disabled', false);
}

function handleClickOnAd() {
  var announcement_id = $(this).find($(".card-body .announcement-id")).html();
  location.href = display_announcement_url + "?announcement_id=" + announcement_id;
}

function loadDetailedSearch() {
  // load detailed search form
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      $("#car-search-form").html(this.responseText)
      $("#id_brand").change(loadModels);
    }
  };
  var url = "search_car/detailed_search";
  xhttp.open("GET", url, true);
  xhttp.send();
}

// search form
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    $("#car-search-form").html(this.responseText)
    $("#id_brand").change(loadModels);
    $("#detailed-search-btn").click(loadDetailedSearch);
  }
};
var url = "search_car/quick_search";
xhttp.open("GET", url, true);
xhttp.send();

// latest upload results
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    $("#search-results-div").html(this.responseText)
    // make them clickable
    announcement_card = $(".search-results .card");
    announcement_card.click(handleClickOnAd);
    announcement_card.css("cursor", "pointer");
  }
};
var url = "search_car/latest_records?page=1";
xhttp.open("GET", url, true);
xhttp.send();
