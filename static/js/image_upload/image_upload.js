function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function renameImage(file) {
  if(!$("#my-dropzone")[0].dropzone.ordinal) {
    $("#my-dropzone")[0].dropzone.ordinal = 1;
  }
  else {
    $("#my-dropzone")[0].dropzone.ordinal++;
  }
  file.ordinal = $("#my-dropzone")[0].dropzone.ordinal - 1;
  // image name format: announcement_id_ordinal_file.name
  return announcement_id + "_" + parseInt($("#my-dropzone")[0].dropzone.ordinal - 1) + "_" + file.name;
}

function rotateImage(event, file) {
  var direction = event.target.id == "rotateLeftBtn" ? -1 : 1
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var img = file.previewTemplate.querySelector("img");
      var deg = img.style.transform.match(/(\d+)/g);
      var sign = img.style.transform.match(/-/g);
      deg = deg != null ? deg[0] : "0";
      sign = sign != null ? sign : "";
      var value = parseInt(sign + deg) % 360 + 90*direction;

      img.style.transform = 'rotate(' + value + 'deg)';
      file.previewTemplate.querySelector('#rotateLeftBtn').disabled = false;
      file.previewTemplate.querySelector('#removeImageBtn').disabled = false;
      file.previewTemplate.querySelector('#rotateRightBtn').disabled = false;
    }
  };
  var url = "image_rotate?image_name=" + file.name +
      "&announcement_id=" + announcement_id +
      "&ordinal=" + file.ordinal +
      "&directon=" + direction;
  xhttp.open("GET", url, true);
  xhttp.send();
  file.previewTemplate.querySelector('#rotateLeftBtn').disabled = true;
  file.previewTemplate.querySelector('#removeImageBtn').disabled = true;
  file.previewTemplate.querySelector('#rotateRightBtn').disabled = true;
}

function removeImage(event, file) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      $("#my-dropzone")[0].dropzone.removeFile(file);
      file.previewTemplate.querySelector('#rotateLeftBtn').disabled = false;
      file.previewTemplate.querySelector('#removeImageBtn').disabled = false;
      file.previewTemplate.querySelector('#rotateRightBtn').disabled = false;
    }
  };
  var url = "image_remove?announcement_id=" + announcement_id + "&ordinal=" + file.ordinal
  xhttp.open("GET", url, true);
  xhttp.send();
  file.previewTemplate.querySelector('#rotateLeftBtn').disabled = true;
  file.previewTemplate.querySelector('#removeImageBtn').disabled = true;
  file.previewTemplate.querySelector('#rotateRightBtn').disabled = true;
}

$(document).ready( function() {

  Dropzone.autoDiscover = false;

  var messageContainer = document.querySelector(".dropzone-message-container");
  var messageDiv = messageContainer.innerHTML;
  messageContainer.parentNode.removeChild(messageContainer);

  // take everything from templateContainer and save it in previewTemplate
  var templateContainer = document.querySelector(".previews-container");
  var previewTemplate = templateContainer.innerHTML;
  // remove it, so it's not visible until we upload a photo
  templateContainer.innerHTML = messageDiv;

  var myDropzone = new Dropzone(document.querySelector("#my-dropzone"), {
    url: "/image_upload/",  // url for upload
    paramName: "image",
    thumbnailWidth: 135,
    thumbnailHeight: 135,
    parallelUploads: 20,
    previewTemplate: previewTemplate,
    previewsContainer: ".previews-container", // Define the container to display the previews
    clickable: templateContainer, // Define the element that should be used as click trigger to select files.
    headers: { 'X-CSRFToken': getCookie('csrftoken')},
    renameFile: renameImage,
    sending: function(file, xhr, formData) {
      formData.append("ordinal", file.ordinal);
    }
  });

  myDropzone.on("addedfile", function(event){
    previewsContainer = document.querySelector(".previews-container");
    dropzoneMessage = document.querySelector(".dropzone-message");
    $(".dropzone-message").css("display", "none");
    $(".previews-container").css("justify-content", "left");
  });

  myDropzone.on("queuecomplete", function(file){
    previews = document.querySelectorAll('.dz-preview');
    previews.forEach(preview => preview.addEventListener('click', function(event) {
      event.stopPropagation();
    }));
  });

  myDropzone.on("thumbnail", function(file){
    var btnRotateLeft = file.previewTemplate.querySelector('#rotateLeftBtn');
    var btnRotateRight = file.previewTemplate.querySelector('#rotateRightBtn');
    var btnRemmoveImg = file.previewTemplate.querySelector('#removeImageBtn');
    btnRotateLeft.addEventListener('click', function(){rotateImage(event, file)});
    btnRotateRight.addEventListener('click', function(){rotateImage(event, file)});
    btnRemmoveImg.addEventListener('click', function(){removeImage(event, file)});
  });

  myDropzone.on("removedfile", function(file) {
    if(!this.files.length) {
      $(".dropzone-message").css("display", "");
    }
  });
})
