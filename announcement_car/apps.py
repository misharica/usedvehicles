from django.apps import AppConfig


class AnnouncementCarConfig(AppConfig):
    name = 'announcement_car'
