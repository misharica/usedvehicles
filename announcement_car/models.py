from django.db import models
from announcement.models import Announcement
from car.models import Car

# Create your models here.

class CarAnnouncement(Announcement):
    car = models.OneToOneField(Car, on_delete=models.CASCADE, null=True)
