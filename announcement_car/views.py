from django.shortcuts import render, get_object_or_404
from django.views import View
from car.forms import CarForm, EngineAndTransmissionForm, PhisicalCharForm, \
                        SecurityAndSafetyForm, MediaForm, AssistanceForm, \
                        OtherFeaturesForm, VehicleHistoryForm
from announcement_car.forms import CarAnnouncementForm
from announcement_car.models import CarAnnouncement
from image_upload.models import CarImageUploadModel
from contact_info.forms import ContactInfoForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from announcement.models import SellerType
from datetime import datetime

from django.http import HttpResponse

ANNOUNCEMENT_ID = 'announcement_id'

# Create your views here.
class AnnounceCarView(LoginRequiredMixin, View):
    """ Define a view to add a new car announcement """

    login_url = '/user_login/login/'

    def get(self, request):

        # check if we already started adding some announcement
        if(not ANNOUNCEMENT_ID in request.session):
            obj, created = CarAnnouncement.objects.get_or_create(user=request.user, submitted=False)
            request.session[ANNOUNCEMENT_ID] = obj.id
        else:
            # for now just delete photos we previously uploaded.
            try:
                car_announcement = CarAnnouncement.objects.get(id=request.session[ANNOUNCEMENT_ID])
                imgs = CarImageUploadModel.objects.filter(announcement=car_announcement)
                for img in imgs:
                    img.image.delete()
                imgs.delete()
            except Exception as e:
                print('Be careful, something went wrong')

        car_form = CarForm()
        eng_tra_form = EngineAndTransmissionForm()
        ph_char_form = PhisicalCharForm()
        ss_form = SecurityAndSafetyForm()
        media_form = MediaForm()
        assistance_form = AssistanceForm()
        other_features_form = OtherFeaturesForm()
        vehicle_hist_form = VehicleHistoryForm()
        car_announcement_form = CarAnnouncementForm()
        ann_cont_info_form = ContactInfoForm()
        announcement_id = request.session[ANNOUNCEMENT_ID]
        context = {'car_form': car_form,
                    'eng_tra_form':eng_tra_form,
                    'ph_char_form': ph_char_form,
                    'ss_form': ss_form,
                    'media_form': media_form,
                    'assistance_form': assistance_form,
                    'other_features_form': other_features_form,
                    'vehicle_hist_form': vehicle_hist_form,
                    'car_announcement_form': car_announcement_form,
                    'ann_cont_info_form': ann_cont_info_form,
                    'announcement_id': announcement_id}
        return render(
                request,
                'announcement_car/announce_car.html',
                context)

    def post(self, request):
        eng_tra_form = EngineAndTransmissionForm(request.POST)
        ph_char_form = PhisicalCharForm(request.POST)
        ss_form = SecurityAndSafetyForm(request.POST)
        media_form = MediaForm(request.POST)
        assistance_form = AssistanceForm(request.POST)
        other_features_form = OtherFeaturesForm(request.POST)
        vehicle_hist_form = VehicleHistoryForm(request.POST)
        ann_cont_info_form = ContactInfoForm(request.POST)
        car_form = CarForm(request.POST)
        car_announcement_form = CarAnnouncementForm(request.POST)

        if not eng_tra_form.is_valid():
            return HttpResponse('eng_tra_form not valid')
        if not ph_char_form.is_valid():
            return HttpResponse('ph_char_form not valid')
        if not ss_form.is_valid():
            return HttpResponse('ss_form not valid')
        if not media_form.is_valid():
            return HttpResponse('media_form not valid')
        if not assistance_form.is_valid():
            return HttpResponse('assistance_form not valid')
        if not other_features_form.is_valid():
            return HttpResponse('other_features_form not valid')
        if not vehicle_hist_form.is_valid():
            return HttpResponse('vehicle_hist_form not valid')
        if not ann_cont_info_form.is_valid():
            return HttpResponse('ann_cont_info_form not valid')
        if not car_form.is_valid():
            return HttpResponse('car_form not valid')
        if not car_announcement_form.is_valid():
            return HttpResponse('car_announcement_form not valid')

        eng_tra_obj = eng_tra_form.save()
        ph_char_obj = ph_char_form.save()
        ss_obj = ss_form.save()
        media_obj = media_form.save()
        assistance_obj = assistance_form.save()
        other_features_obj = other_features_form.save()
        vehicle_hist_obj = vehicle_hist_form.save()
        ann_cont_info_obj = ann_cont_info_form.save()

        car_obj = car_form.save(commit=False)
        car_obj.engine_and_transmission = eng_tra_obj
        car_obj.physical_char = ph_char_obj
        car_obj.security_and_safety = ss_obj
        car_obj.media = media_obj
        car_obj.assistance = assistance_obj
        car_obj.other_features = other_features_obj
        car_obj.vehicle_history = vehicle_hist_obj
        car_obj.save()

        announcement_obj = CarAnnouncement.objects.get(
                id=request.session[ANNOUNCEMENT_ID], submitted=False)
        announcement_obj.seller_type = SellerType.objects.get(id=car_announcement_form['seller_type'].value())
        announcement_obj.description = car_announcement_form['description'].value()
        announcement_obj.price = car_announcement_form['price'].value()
        announcement_obj.fixed_price = car_announcement_form['fixed_price'].value()
        announcement_obj.car = car_obj
        announcement_obj.contact_info = ann_cont_info_obj
        announcement_obj.submitted = True
        announcement_obj.is_active = True
        announcement_obj.date_created = datetime.now()
        announcement_obj.date_updated = datetime.now()
        announcement_obj.save()

        del request.session[ANNOUNCEMENT_ID]

        return render(
            request,
            'announcement_car/announce_posted.html',
            None)

class DisplayAnnouncementView(View):
    """ Define a view to show an existing announcement """

    def get(self, request):
        announcement_id = request.GET.get('announcement_id')
        announcement = get_object_or_404(CarAnnouncement, pk=announcement_id)
        images = CarImageUploadModel.objects.filter(announcement=announcement).order_by('id')
        context = {'announcement': announcement,
                    'images': images}

        return render(
                request,
                'announcement_car/display_announcement.html',
                context)
