from django import forms
from announcement.models import SellerType

class CarAnnouncementForm(forms.Form):
    """ Form to announce a new car """
    seller_type = forms.ModelChoiceField(empty_label = 'Choose...',
            widget=forms.Select, queryset=SellerType.objects.all())
    price = forms.IntegerField(required=False)
    fixed_price = forms.BooleanField(required=False)
    description = forms.CharField(required=False, widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        super(CarAnnouncementForm, self).__init__(*args, **kwargs)
        self.fields['seller_type'].widget.attrs.update({'class': 'form-control'})
        self.fields['price'].widget.attrs.update({'class': 'form-control'})
        self.fields['fixed_price'].widget.attrs.update({'class': 'form-check-input'})
        self.fields['fixed_price'].label = 'Fixed price'
        self.fields['description'].widget.attrs.update({'class': 'form-control'})
        self.fields['description'].widget.attrs.update({'placeholder': 'Enter extra description...'})
        self.fields['description'].widget.attrs.update({'rows': '3'})
