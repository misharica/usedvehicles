"""UsedVehicles URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from announcement_car import views
from car.views import get_car_models
from image_upload.views import image_rotate
from image_upload.views import image_remove

app_name = 'announcement_car'

urlpatterns = [
    path('add_new_announcement/', views.AnnounceCarView.as_view(), name='announce_car'),
    path('add_new_announcement/model_dropdown_list_options/', get_car_models),
    path('add_new_announcement/image_rotate/', image_rotate),
    path('add_new_announcement/image_remove/', image_remove),
    path('display_announcement/', views.DisplayAnnouncementView.as_view(), name='display_announcement')
]
