from django.db import models
from django.contrib.auth import get_user_model
from contact_info.models import AnnouncementContactInfo
from django.utils import timezone

# Create your models here.

class SellerType(models.Model):
    type = models.CharField(max_length=30)
    def __str__(self):
        return self.type

class Announcement(models.Model):

    class Meta:
        abstract = True

    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    contact_info = models.ForeignKey(
            AnnouncementContactInfo, on_delete=models.CASCADE, null=True)
    seller_type = models.ForeignKey(
            SellerType, on_delete=models.CASCADE, null=True)
    submitted = models.BooleanField(default=False)
    date_created = models.DateTimeField(default=timezone.now)
    date_updated = models.DateTimeField(default=timezone.now)
    is_active = models.BooleanField(default=False)
    is_new = models.BooleanField(default=False, null=True)
    description = models.TextField(blank=True, null=True)
    price = models.PositiveIntegerField(default=0, null=True)
    fixed_price = models.BooleanField(null=True)
    promoted = models.BooleanField(null=True)
