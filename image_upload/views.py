from django.shortcuts import render
from django.views import View
from django.http import HttpResponse
from image_upload.models import CarImageUploadModel
from PIL import Image
from image_upload.models import UPLOAD_TO
from image_upload.models import CarImageUploadModel
from django.conf import settings
import os
from announcement_car.views import ANNOUNCEMENT_ID
from announcement_car.models import CarAnnouncement

# Create your views here.
class ImageUploadView(View):
    def get(self, request):
        return render(request, 'image_upload/image_upload.html', None);

    def post(self, request):
        # print(request.FILES.keys()); returns dict_keys(['image']), it can be changed in dropzone.paramName = 'image'
        car_announcement = CarAnnouncement.objects.get(id=request.session[ANNOUNCEMENT_ID])
        model = CarImageUploadModel(
                image=request.FILES['image'],
                announcement=car_announcement,
                ordinal=int(request.POST.get('ordinal')))
        model.save()
        return render(request, 'image_upload/image_upload.html', None);

def image_rotate(request):
    announcement_id = request.GET['announcement_id']
    ordinal = request.GET['ordinal']
    image_name = request.GET['image_name']
    directon = request.GET['directon']

    # image name format: announcement_id_ordinal_imagename
    directory = os.path.join(settings.MEDIA_DIR, UPLOAD_TO)
    image_name = announcement_id + '_' + ordinal + '_' + image_name
    full_image_path = os.path.join(directory, image_name)

    rotation = int(directon) * (-90) # js and python have different sence of direction :)
    image = Image.open(full_image_path)
    image = image.rotate(rotation, expand=True)
    image.save(full_image_path)
    image.close()

    return HttpResponse("OK")

def image_remove(request):
    announcement_id = int(request.GET['announcement_id'])
    ord = int(request.GET['ordinal'])

    img = CarImageUploadModel.objects.get(announcement=announcement_id, ordinal=ord)
    img.image.delete()
    img.delete()

    return HttpResponse("OK")
