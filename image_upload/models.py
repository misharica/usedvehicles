from django.db import models
from announcement_car.models import CarAnnouncement

UPLOAD_TO = 'images'

# Create your models here.
class CarImageUploadModel(models.Model):
    # remove null = True !
    announcement = models.ForeignKey(CarAnnouncement, on_delete=models.CASCADE, null=True)
    ordinal = models.PositiveIntegerField(default=0)
    image = models.ImageField(upload_to=UPLOAD_TO) #maybe define here upload_to
