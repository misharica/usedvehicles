# Generated by Django 2.2.9 on 2020-04-13 07:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('image_upload', '0003_auto_20200408_1518'),
    ]

    operations = [
        migrations.AlterField(
            model_name='carimageuploadmodel',
            name='image',
            field=models.ImageField(upload_to='images'),
        ),
    ]
