import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','UsedVehicles.settings')

import django
django.setup()

from car.models import Brand, Model

FILE_BRAND_NAMES = '/home/rtrk/Documents/LearnPython/python/UsedVehiclesScraping/brand_names.txt'
FILE_BRAND_VALUES = '/home/rtrk/Documents/LearnPython/python/UsedVehiclesScraping/brand_values.txt'
FILE_BRAND_TEMPLATE = '/home/rtrk/Documents/LearnPython/python/UsedVehiclesScraping/%s.txt'

def insert_brands():
    f_brand_names = open(FILE_BRAND_NAMES, 'r')
    for brand_name in f_brand_names:
        if brand_name == '\n':
            continue
        brand_name = brand_name.strip('\n')
        brand_object, created = Brand.objects.get_or_create(name=brand_name)
        if created == True:
            print('Brand %s inserted into Brand table' % brand_object)
        else:
            print('Brand %s already exists in Brand table' % brand_object)
            
    f_brand_names.close()
            
def insert_models():
    f_brand_values = open(FILE_BRAND_VALUES, 'r')
    brand_values_cnt = 0
    for brand_value in f_brand_values:
        if brand_value == '\n':
            continue
        brand_values_cnt = brand_values_cnt + 1;
        brand_value = brand_value.strip('\n')
        #print(brand_value)
        f_specific_brand = open(FILE_BRAND_TEMPLATE % brand_value, 'r')
        
        for model_name in f_specific_brand:
            model_name = model_name.strip('\n')
            found_brand_name = ''
            #print(model_name)
            
            brand_name_cnt = 0
            f_brand_names = open(FILE_BRAND_NAMES, 'r')
            for brand_name in f_brand_names:
                if brand_name == '\n':
                    continue
                brand_name_cnt = brand_name_cnt + 1;
                if brand_name_cnt == brand_values_cnt:
                    found_brand_name = brand_name
                    break
            f_brand_names.close()
            
            found_brand_name = found_brand_name.strip('\n')
            print('Found %s' % found_brand_name)
            brand_object = Brand.objects.get(name=found_brand_name)
            model_object, created = Model.objects.get_or_create(name=model_name, brand = brand_object)
            if created == True:
                print('%d. Model %s inserted into Model table. Foreign key is %s' % (brand_values_cnt, model_object, brand_object))
            else:
                print('%d. Model %s already exists in Model table' % (brand_values_cnt, model_object))
            
        f_specific_brand.close()
    f_brand_values.close()
        
#insert_brands()
insert_models()
