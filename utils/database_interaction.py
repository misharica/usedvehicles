import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','car_test.settings')

import django
django.setup()

from car.models import Car, Make, Model, Media
from Announcement.models import Announcement
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User

#announcements = Announcement.objects.all()
#for announcement in announcements.iterator():
#    print(type(announcement))
#    print(announcement.content_type)


announcement = ContentType.objects.get(app_label='car', model='car')
print(announcement)

print('================================')

announcements = ContentType.objects.filter(app_label='car', model='car')
print(announcements)
for announcement in announcements.iterator():
    print(announcement)
    print(type(announcement))
    print(announcement.model_class())
    make = Make.objects.get(make='BMW')
    user = User.objects.get(username='misharica')
    #print(announcement.get_object_for_this_type(make=make)) # 
    cars = announcement.model_class().objects.filter(user=user) # we will use this; with model_class() we can access the Class we want
    print(cars)
    
print('================================')

make = Make.objects.get(make='BMW')
media = Media.objects.filter(mf_stearing=True, bluetooth=True)
print(media)
announcements = Announcement.objects.filter(content_type__model = 'car', price__gt = 200, price__lt=2000, car__make = make, car__media__in=media)

for announcement in announcements.iterator():
    print(announcement)
    print(type(announcement))
    print(announcement.content_object)




