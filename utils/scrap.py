import time
import requests
from bs4 import BeautifulSoup

BASE_URL = 'https://www.polovniautomobili.com'
TEMPLATE_URL = BASE_URL + "/search/models?category=26&brand=%s&type=full&names=true"
FILE_BRAND_NAMES = 'brand_names.txt'
FILE_BRAND_VALUES = 'brand_values.txt'
FILE_REGIONS = 'regions.txt'


def getBrands():
    
    # clear the files
    open(FILE_BRAND_NAMES, 'w').close()
    open(FILE_BRAND_VALUES, 'w').close()

    page = requests.get(BASE_URL)
    soup = BeautifulSoup(page.content, 'html.parser')
    brands = soup.find(id="brand")

    for brand in brands.find_all('option'):
        f = open(FILE_BRAND_NAMES, "a")
        f.write(brand.get_text() + "\n")
        f.close()

    for brand in brands.find_all('option'):
        f = open(FILE_BRAND_VALUES, "a")
        f.write(brand.get("value") + "\n")
        f.close()
        

def getModels():
    file_brand = open(FILE_BRAND_VALUES, "r")
    for brand_value in file_brand:
        # skip the empty one
        if brand_value == '\n':
            continue
        stripped_value = brand_value.strip('\n')
        print('Sending request for %s' % stripped_value)
        url = TEMPLATE_URL % (stripped_value)
        print('url = %s' % url)
        page = requests.get(url)
        soup = BeautifulSoup(page.content, 'html.parser')
        
        file_name = stripped_value + '.txt'
        print('Opening file %s' % file_name)
        open(file_name, 'w').close()
        file_models = open(file_name, 'a')
        
        for model in soup.find_all('option'):
            file_models.write(model.get_text().strip('\n').strip('\t'))
            print('Inserting %s' % "".join(model.get_text().split()))
        
        print('Closing %s' % file_name)
        file_models.close()
        #break
        print('sleep to confuse the server zzz...')
        time.sleep(2)
    
    file_brand.close()


def getRegions():
    open(FILE_REGIONS, 'w').close()
    
    page = requests.get(BASE_URL)
    soup = BeautifulSoup(page.content, 'html.parser')
    regions = soup.find(id="region")
    print(regions.prettify())
    for region in regions.find_all('option'):
        f = open(FILE_REGIONS, "a")
        f.write(region.get_text() + "\n")
        f.close()
        print(region.get_text() + "\n")


getBrands()
#getModels()
getRegions()

#print('misko zzz...')
#time.sleep(3)
#print('misko good morning :)')