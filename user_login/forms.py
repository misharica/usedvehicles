from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate, get_user_model

class LoginForm(forms.Form):
    """ Form to log in a user using email and password """

    email = forms.EmailField(max_length=254)
    password = forms.CharField(widget=forms.PasswordInput)

    error_messages = {
        'invalid_login': _("Please enter a correct %(email)s and password. "
        "Both fields are case-sensitive."),
        'inactive': _("This account is inactive."),
    }

    def __init__(self, request=None, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.request = request
        self.user_cache = None

        # USERNAME_FIELD defined in user_registration models.py
        UserModel = get_user_model()
        self.email_field = UserModel._meta.get_field(UserModel.USERNAME_FIELD)

        fields_dict = {'email': 'Email address',
                            'password': 'Password'}
        for key in fields_dict:
            self.fields[key].widget.attrs.update({'placeholder': fields_dict[key]})
            self.fields[key].widget.attrs.update({'class': 'form-control'})
            self.fields[key].widget.attrs.update({'id': key})


    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

        if email and password:
            # using email instead of username defined in USERNAME_FIELD
            self.user_cache = authenticate(username=email, password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'email': self.email_field.verbose_name},
                )
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def confirm_login_allowed(self, user):
        """ Controls whether the given User may log in """

        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )
