from django.shortcuts import render, redirect, reverse
from django.views import View
from .forms import LoginForm
from django.contrib.auth import login, logout

from django.http import HttpResponse

# Create your views here.
class LoginView(View):
    """ Define a view to log in user. """

    def get(self, request):
        login_form = LoginForm()
        return render(
                request,
                'user_login/user_login.html',
                {'login_form':  login_form})

    def post(self, request):
        # first argument is request bc we defined it in order to save it
        login_form = LoginForm(request, request.POST)
        if login_form.is_valid():
            login(request, login_form.user_cache)
            if(self.request.GET.get('next')):
                return redirect(self.request.GET.get('next'))
            else:
                return redirect(reverse('home_page:home'))

        error_msgs = login_form.errors['__all__']
        context = {'login_form': login_form, 'error_msgs': error_msgs}
        return render(
                request,
                'user_login/user_login.html',
                context)

class LogoutView(View):
    """ Define a view to logout user. """

    def get(self, request):
        logout(request)
        return redirect(reverse('home_page:home'))
