from django.shortcuts import render, redirect, reverse
from django.views import View
from . import forms

from django.http import HttpResponse
from django.core.mail import send_mail

# Create your views here.

class RegisterUserView(View):
    """ Define a view to register a new user. """

    def get(self, request):
        # ako je ulogovan samo ga forwardujemo na index.html.
        # proveri da li is_authenticated ostaje na true nakon logout-a
        #print(request.user.is_authenticated)
        register_user_form = forms.RegisterUserForm()
        #for key in request.session.keys():
        #    print ("sesion[" + key + "] = " + request.session[key])
        #print(request.user)
        return render(
                request,
                'user_registration/user_registration.html',
                {'register_user_form':  register_user_form})

    def post(self, request):
        register_user_form = forms.RegisterUserForm(request.POST)
        if register_user_form.is_valid():
            register_user_form.save()
            send_mail(
                'Welcome to UsedVehicles',
                'Thank you for choosing UsedVehicles. Please verify your email address to activate your account.',
                'UsedVehicles',
                ['misharica@gmail.com'],
                fail_silently=False,
            )
            # 'login' referes to view name define in urls.py
            return redirect(reverse('user_registration:reg_confirmation'))

        # if form is not valid check errors:

        #if we got have email error it will be rendered, else exception thrown
        try:
            error_msgs = register_user_form.errors['email']
            context = {'register_user_form': register_user_form,
            'error_msgs': error_msgs}
            return render(
                    request,
                    'user_registration/user_registration.html',
                    context)
        except Exception as e:
            pass

        #if we got have pass error it will be rendered, else exception thrown
        try:
            error_msgs = register_user_form.errors['password2']
            context = {'register_user_form': register_user_form,
            'error_msgs': error_msgs}
            return render(
                    request,
                    'user_registration/user_registration.html',
                    context)
        except Exception as e:
            pass

        context = {'register_user_form': register_user_form,
        'error_msgs': None}
        return render(
                request,
                'user_registration/user_registration.html',
                context)

class RegistrationConfirmationView(View):
    def get(self, request):
        # return HttpResponse("Registration successful! Please check your email to activate your account...")
        return render(
            request,
            'user_logout/user_logout.html',
            None)
