from user_registration.models import User
from django.contrib.auth.forms import UserCreationForm

class RegisterUserForm(UserCreationForm):

    # remove help_text and labels, and add placeholders
    def __init__(self, *args, **kwargs):
        super(RegisterUserForm, self).__init__(*args, **kwargs)
        fields_dict = {'email': 'Email address',
                            'password1': 'Password',
                            'password2': 'Confirm password'}
        for key in fields_dict:
            self.fields[key].help_text = None
            self.fields[key].label = ''
            self.fields[key].widget.attrs.update({'placeholder': fields_dict[key]})
            self.fields[key].widget.attrs.update({'class': 'form-control'})
            self.fields[key].widget.attrs.update({'id': key})

    error_messages = {'password_mismatch': "Passwords didn't match."}

        # class, placeholder... are all athributes for html tag
        # this way we can add a class recognized by bootstrap
        # self.fields['first_name'].widget.attrs.update({'class': 'form-control'})

    # for the form take the original user, without any changes
    # if we need to make some changes - >example with UserCreationForm
    # https://docs.djangoproject.com/en/1.8/_modules/django/contrib/auth/forms/
    class Meta:
        model = User
        fields = ('email', 'password1', 'password2')
