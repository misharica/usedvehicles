from django.test import TestCase
from contact_info.models import UserContactInfo, Region
from phonenumber_field.modelfields import PhoneNumberField
from phonenumber_field.phonenumber import PhoneNumber

FILE_REGIONS = '/home/rtrk/Documents/LearnPython/python/UsedVehicles/utils/regions.txt'

# Create your tests here.
class AddRegions(TestCase):
    f_regions = open(FILE_REGIONS, 'r')
    for region in f_regions:
        region = region.strip('\n')
        region_obj, created = Region.objects.get_or_create(name=region)
        if created == True:
            print('Region %s inserted into Region table' % region_obj)
        else:
            print('Region %s already exists in Region table' % region_obj)
    f_regions.close()

class UserContactInfoTestCase(TestCase):
    print('Starting new test...')
    uci = UserContactInfo()
    uci.address = 'Turgenjeva 2'
    uci.city = 'Beograd'
    uci.region = Region.objects.get(name='Beograd')
    uci.code_postal = 11000
    uci.phone1 = '+12125552368'
#    uci.save()
