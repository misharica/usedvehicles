from django.shortcuts import render, redirect, reverse
from django.views import View
from contact_info.forms import ContactInfoForm
from contact_info.models import UserContactInfo, Region, Country
from django.contrib.auth.mixins import LoginRequiredMixin

from django.http import HttpResponse

# Create your views here.
class ShowContactInfoView(LoginRequiredMixin, View):

	def get(self, request):
		user_contact_info = None;
		try:
			user_contact_info = UserContactInfo.objects.get(user=request.user)
		except Exception as e:
			pass

		contact_info_form = ContactInfoForm()
		# If UserContactInfo exists in database use this to prefill the form
		if user_contact_info is not None:
			if request.user.first_name is not None:
				contact_info_form.fields['first_name'].widget.attrs.update({'value': request.user.first_name})
			if request.user.last_name is not None:
				contact_info_form.fields['last_name'].widget.attrs.update({'value': request.user.last_name})
			if user_contact_info.address is not None:
				contact_info_form.fields['address'].widget.attrs.update({'value': user_contact_info.address})
			if user_contact_info.city is not None:
				contact_info_form.fields['city'].widget.attrs.update({'value': user_contact_info.city})
			if user_contact_info.code_postal is not None:
				contact_info_form.fields['code_postal'].widget.attrs.update({'value': user_contact_info.code_postal})
			if user_contact_info.phone1 != "":
				if user_contact_info.phone1.country_code is not None:
					contact_info_form.fields['phone1'].initial= '+' + str(user_contact_info.phone1.country_code)
				if user_contact_info.phone1.national_number is not None:
					contact_info_form.fields['phone1'].widget.attrs.update({'value': user_contact_info.phone1.national_number})
			if user_contact_info.phone2 != "":
				if user_contact_info.phone2.country_code is not None:
					contact_info_form.fields['phone2'].initial= '+' + str(user_contact_info.phone2.country_code)
				if user_contact_info.phone2.national_number is not None:
					contact_info_form.fields['phone2'].widget.attrs.update({'value': user_contact_info.phone2.national_number})

			try:
				region = Region.objects.get(id=user_contact_info.region_id)
				if region is not None:
					contact_info_form.fields['region'].initial = user_contact_info.region_id
			except Exception as e:
				pass

			try:
				country = Country.objects.get(id=user_contact_info.country_id)
				if country is not None:
					contact_info_form.fields['country'].initial = user_contact_info.country_id
			except Exception as e:
				pass
			

		return render(request, 'contact_info/show_user_info.html', {'contact_info_form':contact_info_form})


	def post(self, request):
		
		user_contact_info = None;
		contact_info_form = ContactInfoForm(request.POST)

		if not contact_info_form.is_valid():
			context = {'contact_info_form':contact_info_form}
			return render(request, 'contact_info/show_user_info.html', context)

		try:
			user_contact_info, created = UserContactInfo.objects.get_or_create(user=request.user)
		except Exception as e:
			pass


		request.user.first_name = contact_info_form['first_name'].value()
		request.user.last_name = contact_info_form['last_name'].value()
		request.user.save()

		user_contact_info.address = contact_info_form['address'].value()
		user_contact_info.city = contact_info_form['city'].value()

		# if code_postal is empty form returns string, convert it in None
		if contact_info_form['code_postal'].value() == '':
			user_contact_info.code_postal = None
		else:
			user_contact_info.code_postal = contact_info_form['code_postal'].value()
		user_contact_info.phone1 = contact_info_form['phone1'].value()
		user_contact_info.phone2 = contact_info_form['phone2'].value()

		try:
			user_contact_info.region = Region.objects.get(id=contact_info_form['region'].value())
		except Exception as e:
			user_contact_info.region = None
		
		try:
			user_contact_info.country = Country.objects.get(id=contact_info_form['country'].value())
		except Exception as e:
			user_contact_info.country = None

		user_contact_info.save()

		return redirect(reverse('contact_info:profile_settings'))