from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from django.contrib.auth import get_user_model

class Region(models.Model):
    name = models.CharField(max_length=64)
    def __str__(self):
        return self.name

class Country(models.Model):
    name = models.CharField(max_length=64)
    def __str__(self):
        return self.name

# Create your models here.
class UserContactInfo(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    address = models.CharField(max_length=128)
    city = models.CharField(max_length=128)
    region = models.ForeignKey(Region, on_delete=models.CASCADE, blank=True, null=True)
    country = models.ForeignKey(Country, on_delete=models.CASCADE, blank=True, null=True)
    code_postal = models.PositiveIntegerField(default=0, blank=True, null=True)
    phone1 = PhoneNumberField()
    phone2 = PhoneNumberField(blank=True)

class AnnouncementContactInfo(models.Model):
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)
    address = models.CharField(max_length=128)
    city = models.CharField(max_length=128)
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    code_postal = models.PositiveIntegerField(default=0)
    phone1 = PhoneNumberField()
    phone2 = PhoneNumberField(blank=True)
