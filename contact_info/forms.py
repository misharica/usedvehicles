from django import forms
from contact_info.models import Region, Country, AnnouncementContactInfo
from phonenumber_field.formfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberPrefixWidget

class ContactInfoForm(forms.ModelForm):
    first_name = forms.CharField(max_length=128, required = False)
    last_name = forms.CharField(max_length=128, required = False)
    address = forms.CharField(max_length=128, required = False)
    city = forms.CharField(max_length=128, required = False)
    region = forms.ModelChoiceField(empty_label = 'Choose...',
            widget=forms.Select, queryset=Region.objects.all(), required = False)
    country = forms.ModelChoiceField(empty_label = 'Choose...',
            widget=forms.Select, queryset=Country.objects.all(), required = False)
    code_postal = forms.IntegerField(min_value=0, required = False)
    phone1 = PhoneNumberField(widget=PhoneNumberPrefixWidget, required = False)
    phone2 = PhoneNumberField(widget=PhoneNumberPrefixWidget, required=False)

    class Meta:
        model = AnnouncementContactInfo
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(ContactInfoForm, self).__init__(*args, **kwargs)
        fields_list = {'first_name': 'First name',
                        'last_name': 'Last name',
                        'address': 'Address',
                        'city': 'City',
                        'region': 'Region',
                        'country': 'Country',
                        'code_postal': 'Postal code',
                        'phone1': '55 555-555',
                        'phone2': '55 555-555'}
        for key in fields_list:
            self.fields[key].widget.attrs.update({'class': 'form-control'})
            if key == 'region' or key == 'country':
                continue
            self.fields[key].widget.attrs.update({'placeholder': fields_list[key]})
