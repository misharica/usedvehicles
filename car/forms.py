from django import forms
from car.models import Brand, Model, EmissionClass, Transmission, Drive, \
                        FuelType, BodyType, Color, Airbag, CruiseControl, \
                        AirCondition, EngineAndTransmission, PhysicalCharacteristics, \
                        SecurityAndSafety, Media, Assistance, OtherFeatures, \
                        VehicleHistory, Car
import datetime

class EngineAndTransmissionForm(forms.ModelForm):
        cubic_capacity = forms.IntegerField(min_value=0)
        power = forms.IntegerField(min_value=0)
        emission_class = forms.ModelChoiceField(empty_label = 'Choose...',
                widget=forms.Select, queryset=EmissionClass.objects.all())
        transmission = forms.ModelChoiceField(empty_label = 'Choose...',
                widget=forms.Select, queryset=Transmission.objects.all())
        drive = forms.ModelChoiceField(empty_label = 'Choose...',
                widget=forms.Select, queryset=Drive.objects.all())

        class Meta():
            model = EngineAndTransmission
            fields = '__all__'

        def __init__(self, *args, **kwargs):
            super(EngineAndTransmissionForm, self).__init__(*args, **kwargs)
            fields_list = ['cubic_capacity', 'power', 'emission_class',
                    'transmission', 'drive']

            for name in fields_list:
                self.fields[name].widget.attrs.update({'class': 'form-control'})

            self.fields['cubic_capacity'].widget.attrs.update({'placeholder': 'Cubic capacity'})
            self.fields['power'].widget.attrs.update({'placeholder': 'Power'})

class PhisicalCharForm(forms.ModelForm):
    body_color = forms.ModelChoiceField(empty_label = 'Choose...',
            widget=forms.Select, queryset=Color.objects.all())
    metalic_color = forms.BooleanField(required=False)
    door_number = forms.IntegerField(min_value=0)
    seats_number = forms.IntegerField(min_value=0)

    class Meta():
        model = PhysicalCharacteristics
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(PhisicalCharForm, self).__init__(*args, **kwargs)
        fields_list = ['body_color', 'metalic_color', 'door_number', 'seats_number']

        for name in fields_list:
            if name == 'metalic_color':
                continue
            self.fields[name].widget.attrs.update({'class': 'form-control'})

        self.fields['metalic_color'].widget.attrs.update({'class': 'form-check-input'})
        self.fields['metalic_color'].label = 'Metalic'
        self.fields['door_number'].widget.attrs.update({'placeholder': 'Number of doors'})
        self.fields['seats_number'].widget.attrs.update({'placeholder': 'Number of seats'})

class SecurityAndSafetyForm(forms.ModelForm):
    # Security
    alarm = forms.BooleanField(required=False)
    mechanical_protection = forms.BooleanField(required=False)
    central_lock = forms.BooleanField(required=False)
    keyless_central_lock = forms.BooleanField(required=False)
    # Safety
    airbag = forms.ModelChoiceField(empty_label = 'Choose...',
            widget=forms.Select, queryset=Airbag.objects.all())
    abs = forms.BooleanField(required=False)
    asr = forms.BooleanField(required=False)
    esp = forms.BooleanField(required=False)
    child_lock = forms.BooleanField(required=False)
    blind_spot_assist = forms.BooleanField(required=False)
    line_change_assist = forms.BooleanField(required=False)

    class Meta():
        model = SecurityAndSafety
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(SecurityAndSafetyForm, self).__init__(*args, **kwargs)
        fields_list = {'alarm': 'Alarm',
                        'mechanical_protection': 'Mechanical protection',
                        'central_lock': 'Central locking',
                        'keyless_central_lock': 'Keyless central locking',
                        'airbag': 'Airbags',
                        'abs': 'ABS',
                        'asr': 'ASR',
                        'esp': 'ESP',
                        'child_lock': 'Child lock',
                        'blind_spot_assist': 'Blind spot assistance',
                        'line_change_assist': 'Line change assistance'}

        for key in fields_list:
            if key == 'airbag':
                continue
            self.fields[key].widget.attrs.update({'class': 'form-check-input'})
            self.fields[key].label = fields_list[key]

        self.fields['airbag'].widget.attrs.update({'class': 'form-control'})

class MediaForm(forms.ModelForm):
    mf_stearing = forms.BooleanField(required=False)
    cd_player = forms.BooleanField(required=False)
    on_board_comp = forms.BooleanField(required=False)
    navigation = forms.BooleanField(required=False)
    bluetooth = forms.BooleanField(required=False)
    touchscreen = forms.BooleanField(required=False)
    hands_free = forms.BooleanField(required=False)
    android_auto = forms.BooleanField(required=False)
    apple_carplay = forms.BooleanField(required=False)
    wireless_phone_charger = forms.BooleanField(required=False)

    class Meta():
        model = Media
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(MediaForm, self).__init__(*args, **kwargs)
        fields_list = {'mf_stearing': 'Multifunctional steering wheel',
                        'cd_player': 'CD player',
                        'on_board_comp': 'On board computer',
                        'navigation': 'Navigation',
                        'bluetooth': 'Bluetooth',
                        'touchscreen': 'Touchscreen',
                        'hands_free': 'Hands-free',
                        'android_auto': 'Android auto',
                        'apple_carplay': 'Apple carplay',
                        'wireless_phone_charger': 'Wireless phone charger'}
        for key in fields_list:
            self.fields[key].widget.attrs.update({'class': 'form-check-input'})
            self.fields[key].label = fields_list[key]

class AssistanceForm(forms.ModelForm):
    cruise_ctrl = forms.ModelChoiceField(empty_label = 'Choose...',
            widget=forms.Select, queryset=CruiseControl.objects.all())
    pwr_steering = forms.BooleanField(required=False)
    electric_windows_front = forms.BooleanField(required=False)
    electric_windows_rear = forms.BooleanField(required=False)
    electric_side_mirrors = forms.BooleanField(required=False)
    electric_seats_adj = forms.BooleanField(required=False)
    start_stop = forms.BooleanField(required=False)
    rain_sensor = forms.BooleanField(required=False)
    front_park = forms.BooleanField(required=False)
    rear_park = forms.BooleanField(required=False)
    camera_360 = forms.BooleanField(required=False)
    hill_assist = forms.BooleanField(required=False)
    keyless_start = forms.BooleanField(required=False)

    class Meta():
        model = Assistance
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(AssistanceForm, self).__init__(*args, **kwargs)
        fields_list = {'cruise_ctrl': 'Cruise control',
                        'pwr_steering': 'Power steering wheel',
                        'electric_windows_front': 'Electric windows front',
                        'electric_windows_rear': 'Electric windows rear',
                        'electric_side_mirrors': 'Electric side mirrors',
                        'electric_seats_adj': 'Electric seats adjustment',
                        'start_stop': 'Start-stop system',
                        'rain_sensor': 'Rain sensor',
                        'front_park': 'Parking sensors front',
                        'rear_park': 'Parking sensors rear',
                        'camera_360': 'Camera 360',
                        'hill_assist': 'Hill assist system',
                        'keyless_start': 'Keyless engine start'}
        for key in fields_list:
            if key == 'cruise_ctrl':
                continue
            self.fields[key].widget.attrs.update({'class': 'form-check-input'})
            self.fields[key].label = fields_list[key]

        self.fields['cruise_ctrl'].widget.attrs.update({'class': 'form-control'})

class OtherFeaturesForm(forms.ModelForm):
        aircondition = forms.ModelChoiceField(empty_label = 'Choose...',
                widget=forms.Select, queryset=AirCondition.objects.all())
        arm_rest = forms.BooleanField(required=False)
        heated_seats_front = forms.BooleanField(required=False)
        heated_seats_back = forms.BooleanField(required=False)
        xenon_headlights = forms.BooleanField(required=False)
        led_headlights = forms.BooleanField(required=False)
        led_backlights = forms.BooleanField(required=False)
        panoramic_roof = forms.BooleanField(required=False)
        sunroof = forms.BooleanField(required=False)
        alloy_wheels = forms.BooleanField(required=False)
        steel_wheels = forms.BooleanField(required=False)
        summer_tyres = forms.BooleanField(required=False)
        winter_tyres = forms.BooleanField(required=False)
        all_season_tyres = forms.BooleanField(required=False)

        class Meta():
            model = OtherFeatures
            fields = '__all__'

        def __init__(self, *args, **kwargs):
            super(OtherFeaturesForm, self).__init__(*args, **kwargs)
            fields_list = {'aircondition': 'Aircondition',
                            'arm_rest': 'Arm rest',
                            'heated_seats_front': 'Heated seats front',
                            'heated_seats_back': 'Heated seats back',
                            'xenon_headlights': 'Xenon headlights',
                            'led_headlights': 'LED headlights',
                            'led_backlights': 'LED backlights',
                            'panoramic_roof': 'Panoramic roof',
                            'sunroof': 'Sunroof',
                            'alloy_wheels': 'Alloy wheels',
                            'steel_wheels': 'Steel wheels',
                            'summer_tyres': 'Summer tyres',
                            'winter_tyres': 'Winter tyres',
                            'all_season_tyres': 'All season tyres'}
            for key in fields_list:
                if key == 'aircondition':
                    continue
                self.fields[key].widget.attrs.update({'class': 'form-check-input'})
                self.fields[key].label = fields_list[key]

            self.fields['aircondition'].widget.attrs.update({'class': 'form-control'})

class VehicleHistoryForm(forms.ModelForm):
        first_owner = forms.BooleanField(required=False)
        full_service_hist = forms.BooleanField(required=False)
        demaged = forms.BooleanField(required=False)
        garaged = forms.BooleanField(required=False)
        warranty = forms.BooleanField(required=False)
        disabled_access = forms.BooleanField(required=False)
        non_smoker = forms.BooleanField(required=False)

        class Meta():
            model = VehicleHistory
            fields = '__all__'

        def __init__(self, *args, **kwargs):
            super(VehicleHistoryForm, self).__init__(*args, **kwargs)
            fields_list = {'first_owner': 'First owner',
                            'full_service_hist': 'Full serice history',
                            'demaged': 'Demaged vehicle',
                            'garaged': 'Garaged vehicle',
                            'warranty': 'Warranty',
                            'disabled_access': 'Dissabled access',
                            'non_smoker': 'Non-smoker vehicle'}
            for key in fields_list:
                self.fields[key].widget.attrs.update({'class': 'form-check-input'})
                self.fields[key].label = fields_list[key]

class CarForm(forms.ModelForm):
    """ Form to populate a new car """

    brand = forms.ModelChoiceField(empty_label = 'Choose...',
            widget=forms.Select, queryset=Brand.objects.all())
    model = forms.ModelChoiceField(empty_label = 'Choose...',
            widget=forms.Select, queryset=Model.objects.all())
    first_registration = forms.IntegerField(
            min_value=1900, max_value=datetime.datetime.now().year)
    kilometerage = forms.IntegerField(min_value=0)
    fuel_type = forms.ModelChoiceField(empty_label = 'Choose...',
            widget=forms.Select, queryset=FuelType.objects.all())
    body_type = forms.ModelChoiceField(empty_label = 'Choose...',
            widget=forms.Select, queryset=BodyType.objects.all())

    class Meta():
        model = Car
        exclude = ['engine_and_transmission', 'physical_char', 'security_and_safety', \
                'media', 'assistance', 'other_features', 'vehicle_history']

    def __init__(self, *args, **kwargs):
        super(CarForm, self).__init__(*args, **kwargs)
        fields_list = ['brand', 'model', 'first_registration', 'kilometerage',
                'fuel_type', 'body_type']

        for name in fields_list:
            self.fields[name].widget.attrs.update({'class': 'form-control'})
            self.fields[name].widget.attrs.update({'required': 'True'})

        self.fields['model'].widget.attrs['disabled'] = "disabled"

        self.fields['first_registration'].widget.attrs.update({'placeholder': 'First registration'})
        self.fields['kilometerage'].widget.attrs.update({'placeholder': 'Kilometerage'})
