from django.db import models

# Create your models here.

################
# Quick search #
################
class Brand(models.Model):
    name = models.CharField(max_length=30)
    def __str__(self):
        return self.name

class Model(models.Model):
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    def __str__(self):
        return self.name

class BodyType(models.Model):
    type = models.CharField(max_length=30)
    def __str__(self):
        return self.type

class FuelType(models.Model):
    type = models.CharField(max_length=30)
    def __str__(self):
        return self.type

###################
# Detailed search #
###################
# EngineAndTransmission
class Transmission(models.Model):
    type = models.CharField(max_length=30)
    def __str__(self):
        return self.type

class Drive(models.Model):
    type = models.CharField(max_length=30)
    def __str__(self):
        return self.type

class EmissionClass(models.Model):
    emission_class = models.CharField(max_length=30)
    def __str__(self):
        return self.emission_class

class EngineAndTransmission(models.Model):
    cubic_capacity = models.PositiveIntegerField(default=0)
    power = models.PositiveIntegerField(default=0)
    emission_class = models.ForeignKey(EmissionClass, on_delete=models.CASCADE)
    transmission = models.ForeignKey(Transmission, on_delete=models.CASCADE)
    drive = models.ForeignKey(Drive, on_delete=models.CASCADE)

# Phisical characteristics
class Color(models.Model):
    name = models.CharField(max_length=30)
    def __str__(self):
        return self.name

class PhysicalCharacteristics(models.Model):
    body_color = models.ForeignKey(Color, on_delete=models.CASCADE)
    metalic_color = models.BooleanField()
    door_number = models.PositiveIntegerField(default=0)
    seats_number = models.PositiveIntegerField(default=0)

# Security and Safety group
class Airbag(models.Model):
    type = models.CharField(max_length=30)
    def __str__(self):
        return self.type

class SecurityAndSafety(models.Model):
    # security
    alarm = models.BooleanField()
    mechanical_protection = models.BooleanField()
    central_lock = models.BooleanField()
    keyless_central_lock = models.BooleanField()
    # safety
    airbag = models.ForeignKey(Airbag, on_delete=models.CASCADE)
    abs = models.BooleanField()
    asr = models.BooleanField()
    esp = models.BooleanField()
    child_lock = models.BooleanField()
    blind_spot_assist = models.BooleanField()
    line_change_assist = models.BooleanField()

# Media
class Media(models.Model):
    mf_stearing = models.BooleanField()
    cd_player = models.BooleanField()
    on_board_comp = models.BooleanField()
    navigation = models.BooleanField()
    bluetooth = models.BooleanField()
    touchscreen = models.BooleanField()
    hands_free = models.BooleanField()
    android_auto = models.BooleanField()
    apple_carplay = models.BooleanField()
    wireless_phone_charger = models.BooleanField()

# Assistance
class CruiseControl(models.Model):
    type = models.CharField(max_length=30)
    def __str__(self):
        return self.type

class Assistance(models.Model):
    cruise_ctrl = models.ForeignKey(CruiseControl, on_delete=models.CASCADE)
    pwr_steering = models.BooleanField()
    electric_windows_front = models.BooleanField()
    electric_windows_rear = models.BooleanField()
    electric_side_mirrors = models.BooleanField()
    electric_seats_adj = models.BooleanField()
    start_stop = models.BooleanField()
    rain_sensor = models.BooleanField()
    front_park = models.BooleanField()
    rear_park = models.BooleanField()
    camera_360 = models.BooleanField()
    hill_assist = models.BooleanField()
    keyless_start = models.BooleanField()

# Other Features
class AirCondition(models.Model):
    type = models.CharField(max_length=30)
    def __str__(self):
        return self.type

class OtherFeatures(models.Model):
    aircondition = models.ForeignKey(AirCondition, on_delete=models.CASCADE)
    arm_rest = models.BooleanField()
    heated_seats_front = models.BooleanField()
    heated_seats_back = models.BooleanField()
    xenon_headlights = models.BooleanField()
    led_headlights = models.BooleanField()
    led_backlights = models.BooleanField()
    panoramic_roof = models.BooleanField()
    sunroof = models.BooleanField()
    alloy_wheels = models.BooleanField()
    steel_wheels = models.BooleanField()
    summer_tyres = models.BooleanField()
    winter_tyres = models.BooleanField()
    all_season_tyres = models.BooleanField()

class VehicleHistory(models.Model):
    first_owner = models.BooleanField()
    full_service_hist = models.BooleanField()
    demaged = models.BooleanField()
    garaged = models.BooleanField()
    warranty = models.BooleanField()
    disabled_access = models.BooleanField()
    non_smoker = models.BooleanField()

###################
# Final Car Model #
###################
class Car(models.Model):
    # Quick search #
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    model = models.ForeignKey(Model, on_delete=models.CASCADE)
    body_type = models.ForeignKey(BodyType, on_delete=models.CASCADE)
    fuel_type = models.ForeignKey(FuelType, on_delete=models.CASCADE)
    kilometerage = models.PositiveIntegerField(default=0)
    first_registration = models.PositiveIntegerField(default=0)

    # Detailed search #
    engine_and_transmission = models.OneToOneField(
            EngineAndTransmission, on_delete=models.CASCADE)
    physical_char = models.OneToOneField(
            PhysicalCharacteristics, on_delete=models.CASCADE)
    security_and_safety = models.OneToOneField(
            SecurityAndSafety, on_delete=models.CASCADE)
    media = models.OneToOneField(Media, on_delete=models.CASCADE)
    assistance = models.OneToOneField(Assistance, on_delete=models.CASCADE)
    other_features = models.OneToOneField(
            OtherFeatures, on_delete=models.CASCADE)
    vehicle_history = models.OneToOneField(
            VehicleHistory, on_delete=models.CASCADE)
