from django.contrib import admin
from car.models import Brand, Model

# Register your models here.
admin.site.register(Brand)
admin.site.register(Model)
