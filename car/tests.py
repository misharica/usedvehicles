from django.test import TestCase
from car.models import FuelType, EmissionClass, Transmission, Drive, BodyType, \
                        Color, AirCondition, Airbag

FILE_FUEL_TYPES = '/home/rtrk/Documents/LearnPython/python/UsedVehicles/utils/fuel_types.txt'
FILE_EM_CLASSES = '/home/rtrk/Documents/LearnPython/python/UsedVehicles/utils/emission_classes.txt'
FILE_TRANSM_TYPES = '/home/rtrk/Documents/LearnPython/python/UsedVehicles/utils/transmission_types.txt'
FILE_DRIVE_TYPES = '/home/rtrk/Documents/LearnPython/python/UsedVehicles/utils/drive_types.txt'
FILE_BODY_TYPES = '/home/rtrk/Documents/LearnPython/python/UsedVehicles/utils/body_types.txt'
FILE_COLORS = '/home/rtrk/Documents/LearnPython/python/UsedVehicles/utils/colors.txt'
FILE_AC = '/home/rtrk/Documents/LearnPython/python/UsedVehicles/utils/airconditions.txt'
FILE_AIRBAGS = '/home/rtrk/Documents/LearnPython/python/UsedVehicles/utils/airbags.txt'


# Create your tests here.
class AddFuelTypes(TestCase):
    f_fuel_types = open(FILE_FUEL_TYPES, 'r')
    for t in f_fuel_types:
        t = t.strip('\n')
        fuel_type_obj, created = FuelType.objects.get_or_create(type=t)
        if created == True:
            print('FuelType %s inserted into FuelType table' % fuel_type_obj)
        else:
            print('FuelType %s already exists in FuelType table' % fuel_type_obj)
    f_fuel_types.close()

class AddEmissionClasses(TestCase):
    f_em_classes = open(FILE_EM_CLASSES, 'r')
    for ecl in f_em_classes:
        ecl = ecl.strip('\n')
        em_class_obj, created = EmissionClass.objects.get_or_create(emission_class=ecl)
        if created == True:
            print('EmissionClass %s inserted into EmissionClass table' % em_class_obj)
        else:
            print('EmissionClass %s already exists in EmissionClass table' % em_class_obj)
    f_em_classes.close()

class AddTransmissions(TestCase):
    f_transm = open(FILE_TRANSM_TYPES, 'r')
    for transm in f_transm:
        transm = transm.strip('\n')
        transm_obj, created = Transmission.objects.get_or_create(type=transm)
        if created == True:
            print('Transmission %s inserted into Transmission table' % transm_obj)
        else:
            print('Transmission %s already exists in Transmission table' % transm_obj)
    f_transm.close()

class AddDrives(TestCase):
    f_drives = open(FILE_DRIVE_TYPES, 'r')
    for drive in f_drives:
        drive = drive.strip('\n')
        drive_obj, created = Drive.objects.get_or_create(type=drive)
        if created == True:
            print('Drive %s inserted into Drive table' % drive_obj)
        else:
            print('Drive %s already exists in Drive table' % drive_obj)
    f_drives.close()

class AddBodyTypes(TestCase):
    f_body_types = open(FILE_BODY_TYPES, 'r')
    for type in f_body_types:
        type = type.strip('\n')
        type_obj, created = BodyType.objects.get_or_create(type=type)
        if created == True:
            print('BodyType %s inserted into BodyType table' % type_obj)
        else:
            print('BodyType %s already exists in BodyType table' % type_obj)
    f_body_types.close()

class AddBodyColors(TestCase):
    f_body_colors = open(FILE_COLORS, 'r')
    for color in f_body_colors:
        color = color.strip('\n')
        color_obj, created = Color.objects.get_or_create(name=color)
        if created == True:
            print('Color %s inserted into Color table' % color_obj)
        else:
            print('Color %s already exists in Color table' % color_obj)
    f_body_colors.close()

class AddAirconditions(TestCase):
    f_acs = open(FILE_AC, 'r')
    for ac in f_acs:
        ac = ac.strip('\n')
        ac_obj, created = AirCondition.objects.get_or_create(type=ac)
        if created == True:
            print('AirCondition %s inserted into AirCondition table' % ac_obj)
        else:
            print('AirCondition %s already exists in AirCondition table' % ac_obj)
    f_acs.close()

class AddAirbags(TestCase):
    f_airbags = open(FILE_AIRBAGS, 'r')
    for airbag in f_airbags:
        airbag = airbag.strip('\n')
        airbag_obj, created = Airbag.objects.get_or_create(type=airbag)
        if created == True:
            print('Airbag %s inserted into Airbag table' % airbag_obj)
        else:
            print('Airbag %s already exists in Airbag table' % airbag_obj)
    f_airbags.close()
