from django.shortcuts import render
from car.models import Model

# Create your views here.
def get_car_models(request):

    brand_id = request.GET.get('brand_id')
    models = Model.objects.filter(brand_id=brand_id).order_by('id')
    return render(request, 'car/model_dropdown_list_options.html', {'models':models})
